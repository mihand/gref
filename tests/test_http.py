from tests.testsconfig import TESTDIR, TESTSRVRDIR, PORT, TESTNONTEXTDIR
import SimpleHTTPServer
import SocketServer
import gref
import logging
import os
import threading
import unittest

LOG = logging.getLogger(__name__)

#todo:
#testing timeouts on http GET will require writing a httprequesthandler
#wich will responde on a url by sleeping 

def listen(httpd):
    httpd.serve_forever()

class Test(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        '''
        starts a test http server
        and sets up the current dir
        '''
        #the simple http server serves from the current dir
        os.chdir(TESTSRVRDIR)
        cls.path = os.path.relpath(TESTDIR)
        
        handler = SimpleHTTPServer.SimpleHTTPRequestHandler
        handler.extensions_map['txt'] = 'text/text'
        #without the line below the PORT will be in TIME_WAIT after socket closes
        #so we cannot rerun the test for a while
        SocketServer.TCPServer.allow_reuse_address = True
        cls.httpd = SocketServer.TCPServer(("", PORT), handler)
        threading.Thread(target=listen, kwargs={'httpd':cls.httpd}).start()

    @classmethod
    def tearDownClass(cls):
        cls.httpd.shutdown()

    def test_integration(self):        
        gref.USE_MIME = True
        gref.USE_GUESS = True
        r = gref.main(self.path, r'a.*bc')
        self.assertEquals(r,
            [ '../data/a           ( 0) : >>abc def ghi abc<<',
              '../data/a           ( 2) : xyz >>abc<<',
              '../data/a -> http://localhost:8000/b.txt( 1) : >>abc<<'
            ])

    def test_server_mime(self):
        gref.USE_GUESS = True
        gref.USE_MIME = False   
        r = gref.main(self.path, r'not binary')
        self.assertEquals(r,
            [ 
              '../data/a -> http://localhost:8000/ddd( 0) : this is >>not binary<<'
            ])
 
        gref.USE_MIME = True
        
        r = gref.main(self.path, r'not binary')
        self.assertEquals(r,
            [])

    def test_nontexts(self):
        gref.USE_MIME = True
        gref.USE_GUESS = True
        r = gref.main(TESTNONTEXTDIR, r'.*')
        self.assertEquals(r,
            [])