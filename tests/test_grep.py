import gref
import os.path
import re
import unittest
from tests.testsconfig import TESTDIR, TESTNONTEXTDIR

class Test(unittest.TestCase):
    def setUp(self):
        self.pth_a = os.path.join(TESTDIR, 'a')
        
    def test_grep_file(self):
        with open(self.pth_a) as fo:
            ms =  gref.grep( fo,  [re.compile('\d+:\d+:\d+')])
            ms = ms[0]
            self.assertEquals(len(ms), 1)
            self.assertEquals(ms[0][1], '02:04:12')
            
    def test_seems_plain_ascii(self):
        with open(self.pth_a) as fo:
            a_lines = fo.read().splitlines()
            
        with open(self.pth_a) as fo:
            plain, lineit = gref.seems_plain_ascii_text(fo)
            self.assertTrue(plain)
            lines = list(lineit)
            self.assertEquals(a_lines, lines)
            
    def test_non_ascii(self):
        with open(os.path.join(TESTNONTEXTDIR, 'picture.png')) as fo:
            plain, _ = gref.seems_plain_ascii_text(fo)
            self.assertFalse(plain)
            