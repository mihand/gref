import os.path

RESOURCEDIR = os.path.split(__file__)[0]
TESTDIR = os.path.join(RESOURCEDIR, 'data')
TESTSRVRDIR = os.path.join(RESOURCEDIR, 'serverdata')
TESTNONTEXTDIR = os.path.join(RESOURCEDIR, 'nontext')
PORT = 8000