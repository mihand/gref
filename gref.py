#!/usr/bin/python
from __future__ import division
import os
import sys
import re
import argparse
import urllib2
from contextlib import closing
import string
import mimetypes

USE_MIME = True
USE_GUESS = True

URL_TIMEOUT = 3
URL_REGEX = re.compile(r"\b(?:(?:https?|ftp|file)://|www\.|ftp\.)"
                       r"[\w\d+-=~_&@#/%|$?!:,.]*[\w\d+&@#/%=~_|$]")

PRINTABLE_ASCII_REGEX = re.compile('[{0}]'.format(string.printable)) 
#ASCII_GUESS_BUFF_LEN = 320 
ASCII_GUESS_BUFF_LEN = 40
ASCII_GUESS_THRESHOLD = 0.8

def seems_plain_ascii_text(filelike):
    'guess based on printable char proportion'
    def lines():
        '''iterates lines from the file like object
         behaves as if it "unread" the char buffer used to guess file type'''
        unreadlines = text.splitlines()
        unreadlines, lastline = unreadlines[:-1], unreadlines[-1]
         
        for l in unreadlines:
            yield l
            
        if text.endswith('\r') or text.endswith('\n'):
            yield lastline
        else:
            try:
                yield lastline + next(filelike).rstrip('\r\n')
            except StopIteration:
                yield lastline
        
        for l in filelike:
            yield l.rstrip('\r\n')
            
    text = filelike.read(ASCII_GUESS_BUFF_LEN)    
    printables = len(PRINTABLE_ASCII_REGEX.findall(text))
    
    is_plain = printables / len(text) > ASCII_GUESS_THRESHOLD
    
    return is_plain, lines()  

def iterfiles(path):
    def onerror(e):
        print >> sys.stderr, 'while listing files :', str(e)
        
    if os.path.isfile(path):
        yield path
    elif os.path.isdir(path):
        for root, dirs, files in os.walk(path,onerror=onerror):
            dirs.sort()
            for f in files:
                yield os.path.join(root, f)
    else:
        print >> sys.stderr, 'path is not valid ', path

def grep(lineit, regexes):
    ret = [ [] for _ in regexes]

    for i, line in enumerate(lineit):
        line = line.rstrip('\r\n')
        for ri, regex in enumerate(regexes):
            matches = list(regex.finditer(line))
            if matches:
                ret[ri].append( (i, line, [m.span() for m in matches]) )
    return ret

def formatmatches(path, matched_lines):
    TEMPL = '{0:<20}({1:>2}) : {2}>>{3}<<{4}'
    res = []
    for i, line, spans in matched_lines:
        for s,e in spans:
            res.append(TEMPL.format(path, i, line[:s], line[s:e], line[e:])) 
    return res

def process_file(f, regex, urls, res):
    if USE_MIME:
        mimetype = mimetypes.guess_type(f)[0]
        if mimetype is not None and not mimetype.startswith('text'):
            print >> sys.stderr, 'not text : ', f
            return

    with open(f) as fo: 
        if USE_GUESS:
            plain, lineit = seems_plain_ascii_text(fo)
        
            if not plain:
                print >> sys.stderr, 'not text : ', f
                return
        else:
            lineit = fo
        
        matched_lines, urllines = grep(lineit, [regex, URL_REGEX])
       
        for _, line, spans in urllines:
            urls += [(f, line[s:e]) for s,e in spans]
        res += formatmatches(f, matched_lines)

def process_url(url, regex, origin, res):
    with closing(urllib2.urlopen(url, timeout=URL_TIMEOUT)) as fo:
        if USE_MIME:
            mimetype = fo.headers.type
            if mimetype is not None and not mimetype.startswith('text'):
                print >> sys.stderr, 'not text : ', url
                return
        if USE_GUESS:        
            plain, lineit = seems_plain_ascii_text(fo)
            if not plain:
                print >> sys.stderr, 'not ascii : ', url
                return
        else:
            lineit = fo
            
        matched_lines = grep(lineit, [regex])[0]
        res += formatmatches(origin + ' -> '+url, matched_lines)
    
def main(path, regex):
    mimetypes.init()
    regex = re.compile(regex)
    urls = []
    res = []

    for f in iterfiles(path):
        try:
            process_file(f, regex, urls, res)
        except IOError as e:
            print >> sys.stderr, 'while reading :', f, str(e)

    for o, url in urls:
        try:
            process_url(url, regex, o, res)
        except IOError as e:
            print >> sys.stderr, 'while reading :', url, str(e)
        
    return res
                        
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="grep a pattern in files and reference url's")
    parser.add_argument('regex', type=str, help='the regex to be searched')
    parser.add_argument('path', type=str, help='the root directory where the files to be searched are located')
    parser.add_argument('--nomime', action='store_false', help='disable mime type filtering')
    parser.add_argument('--noguess', action='store_false', help='disable file type guessing')
    args = parser.parse_args()
    
    USE_MIME = args.nomime
    USE_GUESS = args.noguess
    
    for l in main(args.path, args.regex):
        print l
